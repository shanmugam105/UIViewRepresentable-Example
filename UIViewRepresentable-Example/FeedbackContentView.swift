//
//  FeedbackContentView.swift
//  UIViewRepresentable-Example
//
//  Created by Sparkout on 29/09/22.
//

import SwiftUI

let deviceSize: CGSize = UIScreen.main.bounds.size

struct FeedbackContentView: View {
    @State var feedback: String = ""
    
    var body: some View {
        VStack {
            CustomTextView(text: $feedback, textStyle: UIFont.TextStyle.caption1)
                .frame(width: deviceSize.width, height: 50)
                .border(Color.gray, width: 1.0)
            Button {
                print(feedback)
            } label: {
                 Text("Submit")
            }
        }
    }
}


struct CustomTextView: UIViewRepresentable {
 
    @Binding var text: String
    let textStyle: UIFont.TextStyle
 
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
 
        textView.delegate = context.coordinator
        textView.font = UIFont.preferredFont(forTextStyle: textStyle)
        textView.autocapitalizationType = .sentences
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
 
        return textView
    }
 
    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
        uiView.font = UIFont.preferredFont(forTextStyle: textStyle)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        let parent: CustomTextView
        
        init(parent: CustomTextView) {
            self.parent = parent
        }
     
        func textViewDidChange(_ textView: UITextView) {
            parent.text = textView.text
        }
    }
}
