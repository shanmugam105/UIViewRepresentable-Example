//
//  UIViewRepresentable_ExampleApp.swift
//  UIViewRepresentable-Example
//
//  Created by Sparkout on 29/09/22.
//

import SwiftUI

@main
struct UIViewRepresentable_ExampleApp: App {
    var body: some Scene {
        WindowGroup {
            FeedbackContentView()
        }
    }
}
